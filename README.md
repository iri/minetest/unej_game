# GAME MINETEST UNEJ (FR)

*English version below*

# Contexte
Ce game (jeu) Minetest a été utilisé pour l’expérimentation UNEJ qui s’est déroulée de 2020 à 2024. Il a été constitué au sein de l’[Institut de recherche et d’innovation](https://www.iri.centrepompidou.fr/) avec l’aide de contributeurs extérieurs. L’IRI est une association fondée par le philosophe Bernard Stiegler ayant pour but d’analyser l’évolution des pratiques culturelles dues aux nouvelles technologies. UNEJ, [Urbanités numériques en jeux](https://unej.tac93.fr/) est un projet lancé dans le cadre d’un [programme de recherche](https://tac93.fr/) se déroulant en Seine-Saint-Denis (dans le nord de Paris), qui vise à permettre à des élèves de collèges et lycées de se réapproprier leur territoire : le but étant qu’ils travaillent avec [Minetest](https://www.minetest.net/education/) pour repenser l’aménagement de leur cour de récréation ou du quartier autour de leur établissement scolaire à partir d’une carte Minetest générée via un [service de création de carte à partir des données géographiques du territoire français](https://minecraft.ign.fr/).

# À propos de ce game
Le game unej est donc celui qui a servi aux élèves et enseignants ayant participé à l’expérimentation. Il constitue un ensemble de mods choisis spécifiquement  pour l’occasion (liste complète en dessous) et a pour base le game Minetest par défaut. Les mods sont directement liés à leurs dépôts d’origine via [Git Submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules).
De nombreux autres game Minetest existent et sont trouvables dans la bibliothèque de contenu Minetest dite [ContentDB](https://content.minetest.net/packages/?type=game) créée par [Andrew Ward](https://gitlab.com/rubenwardy).
L’expérimentation UNEJ étant dorénavant terminée, ce game va être archivé mais un nouveau game a été constitué à partir de celui-ci afin de faciliter la reproductibilité du dispositif pour des usages similaire : il s’agit du [game MTL](https://gitlab.com/iri-research-org/urbanum/minetest/mtl_game)

# Contributeurs
Maintien et déploiement: Yves-Marie Haussonne https://github.com/ymph, https://gitlab.com/ymph

Sélection et curation des mods: Riwad Salim (https://github.com/late-coffee

Conseils sur les choix et fonctionnalités des mods :https://gitlab.com/Late-Coffee), Thomas François (https://github.com/ThoMateFarCie) et Meredith Nolot (https://github.com/Lemente, https://gitlab.com/Lemente)

Mise en page et rédaction du readme : Merlin Payant et Riwad Salim

## Installation

- Décompressez l'archive, renommez le dossier en `unej` et placez-le dans .. minetest/games/

- GNU/Linux : Si vous utilisez une installation sur l'ensemble du système, placez dans ~/.minetest/games/.

Le moteur Minetest peut être trouvé sur [GitHub](https://github.com/minetest/minetest).

Pour plus d'informations ou de l'aide, voir :  
https://wiki.minetest.net/Installing_Mods


## Compatibilité

Ce game (jeu) est compatible avec la version et le déploiement de minetest que l'on trouve dans l'[image docker de l'UNEJ](https://gitlab.com/iri-research-org/urbanum/minetest/docker-unej).


## Licences

Voir `LICENSE.txt`

<br><br><br><br>

# UNEJ MINETEST GAME (EN)
# Context 	 	 	
This Minetest game was used for the UNEJ experiment, which ran from 2020 to 2024. The module was created within the [Institute for Research and Innovation](https://www.iri.centrepompidou.fr/) with the help of external contributors. The IRI is an association founded by philosopher Bernard Stiegler to analyze the evolution of cultural practices due to new technologies. UNEJ, [Urbanités numériques en jeux](https://unej.tac93.fr/) is a project launched as part of a [research program](https://tac93.fr/) taking place in Seine-Saint-Denis (north of Paris), which aims to enable middle and high school students to reclaim their territory: the aim being for them to use [Minetest](https://minetest.net/education/) to reimagine and recreate the layouts of their playground and neighborhood based on Minetest interactive maps generated via a map creation service based on French territory geographical data.

# About this game
The UNEJ game is used by students and teachers who took part in the experiment. It consists of a set of mods chosen specifically for the occasion (full list below) and is based on [the default Minetest game](https://github.com/minetest/minetest_game). The mods are linked directly to their original repositories via [Git Submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules).
Many other games within Minetest can be found in the Minetest content library, [ContentDB](https://content.minetest.net/packages/?type=game), created by [Andrew Ward](https://gitlab.com/rubenwardy).
As the UNEJ experiment is now over, this game will be archived, but a new game has been created from it to make it easier to reproduce the system for similar uses: the [game MTL](https://gitlab.com/iri-research-org/urbanum/minetest/mtl_game).

# Contributors
Management and development: Yves-Marie Haussonne https://github.com/ymph, https://gitlab.com/ymph

Selection and curation of mods: Riwad Salim (Late-coffee, https://github.com/late-coffee, https://gitlab.com/Late-Coffee), Thomas François (Thomate) and Meredith Nolot (Lemente)

Advice on mod selection and features: https://gitlab.com/Late-Coffee), Thomas François (https://github.com/ThoMateFarCie) et Meredith Nolot (https://github.com/Lemente, https://gitlab.com/Lemente)

Layout and writing of the readme: Merlin Payant and Riwad Salim

## Installation
- Unzip the archive, rename the folder to `unej` and
place it in .. minetest/games/

- GNU/Linux: If you use a system-wide installation place
    it in ~/.minetest/games/.

The Minetest engine can be found at [GitHub](https://github.com/minetest/minetest).
For further information or help, see:  
https://wiki.minetest.net/Installing_Mods

## Compatibility
This game is compatible with the minetest version and deploiement found in the [UNEJ docker image](https://gitlab.com/iri-research-org/urbanum/minetest/docker-unej).

## Licensing
See `LICENSE.txt`

<br><br><br><br>

## List of additional mods

| NAME                    | SOURCE                                                                             | FORK                                                                   |
|------------------------|------------------------------------------------------------------------------------|------------------------------------------------------------------------|
| 3d_armor-flyswim       | https://github.com/sirrobzeroone/3d_armor_flyswim |    |
| abriglass              | https://github.com/Ezhh/abriglass |    |
| abripanes              | https://github.com/Ezhh/abripanes |    |
| antigrief              | https://bitbucket.org/sorcerykid/antigrief/src/master/ |    |
| areas                  | https://github.com/minetest-mods/areas |    |
| baked_clay             | https://codeberg.org/tenplus1/bakedclay |    |
| basic_materials        | https://github.com/mt-mods/basic_materials |    |
| basic_signs            | https://github.com/mt-mods/basic_signs |    |
| bike                   | https://gitlab.com/h2mm/bike |    |
| bones                  | https://github.com/Bad-Command/bones |    |
| coloredwood            | https://github.com/mt-mods/coloredwood |    |
| display_modpack        | https://github.com/pyrollo/display_modpack |    |
| dreambuilder_hotbar    | https://github.com/mt-mods/dreambuilder_hotbar |    |
| fmod                   | https://github.com/fluxionary/minetest-fmod.git |    |
| folks                  | https://gitlab.com/SonoMichele/folks.git |    |
| formspecs              | https://bitbucket.org/sorcerykid/formspecs/src/master/ |    |
| futil                  | https://github.com/fluxionary/minetest-futil.git |    |
| game_commands          | https://github.com/minetest-mapserver/mapserver |    |
| give_initial_stuff     | https://github.com/minetest-mapserver/mapserver_mod |    |
| hardenedclay           | https://gitlab.com/iri-research-org/urbanum/minetest/minetest-mod-hardenedclay.git |    |
| headanim               | https://github.com/LoneWolfHT/headanim/ |    |
| homedecor_modpack      | https://github.com/mt-mods/homedecor_modpack |    |
| ignfab                 | https://github.com/ignfab-minalac/minalac/tree/master/sources/src/main/java/ignfab |    |
| ilights                | https://github.com/mt-mods/ilights |    |
| itemshelf              | https://github.com/hkzorman/itemshelf |    |
| laptop                 | https://github.com/Gerold55/laptop |    |
| liquid_restriction     | https://github.com/wsor4035/liquid_restriction |    |
| magic_compass          | https://gitlab.com/zughy-friends-minetest/magic-compass | https://gitlab.com/iri-research-org/urbanum/minetest/magic-compass.git |
| mapserver              | https://github.com/minetest-mapserver/mapserver |    |
| mapserver_mod          | https://github.com/minetest-mapserver/mapserver_mod |    |
| maptools               | https://github.com/minetest-mods/maptools |    |
| mese_restriction       | https://github.com/Ezhh/mese_restriction |    |
| mesecons               | https://github.com/minetest-mods/mesecons |    |
| mesecons_x             | https://gitlab.com/deetmit/mesecons_x |    |
| meshport               | https://github.com/random-geek/meshport |    |
| monitoring             | https://github.com/minetest-monitoring/monitoring |    |
| moreblocks             | https://github.com/minetest-mods/moreblocks/ |    |
| motorboat              | https://github.com/APercy/motorboat |    |
| orienteering           | https://repo.or.cz/minetest_orienteering.git |    |
| perplayer_gamemode     | https://github.com/rubenwardy/perplayer_gamemode |    |
| playerfactions         | https://github.com/mt-mods/playerfactions |    |
| replacer               | https://github.com/Sokomine/replacer |    |
| resize player          | https://github.com/LandonAConway/resize |    |
| respawn                | https://gitlab.com/cronvel/mt-respawn |    |
| rules                  | https://github.com/AiTechEye/agreerules |    |
| sailing_kit            | https://github.com/TheTermos/sailing_kit |    |
| server_news            | https://github.com/Ezhh/server_news |    |
| sign_lib               | https://github.com/mt-mods/signs_lib |    |
| skinsdb                | https://github.com/minetest-mods/skinsdb |    |
| spawn                  | https://github.com/minetest-game-mods/spawn |    |
| spectator_mode         | https://github.com/minetest-mods/spectator_mode |    |
| travelnet              | https://github.com/Sokomine/travelnet | https://github.com/mt-mods/travelnet |
| ts_doors               | https://github.com/minetest-mods/ts_doors |    |
| ts_furniture           | https://github.com/minetest-mods/ts_furniture |    |
| ts_workshop            | https://github.com/minetest-mods/ts_workshop |    |
| unej                   | https://gitlab.com/iri-research-org/urbanum/minetest/minetest-mod-unej |    |
| unified_inventory      | https://github.com/minetest-mods/unified_inventory |    |
| unified_inventory_plus | https://github.com/bousket/unified_inventory_plus |    |
| unifieddyes            | https://github.com/mt-mods/unifieddyes |    |
| we_undo                | https://github.com/HybridDog/we_undo |    |
| whitelist              | https://github.com/AntumMT/mod-whitelist.git |    |
| wield3d                | https://github.com/stujones11/wield3d |    |
| windmill               | https://github.com/Sokomine/windmill |    |
| worldedit              | https://github.com/Uberi/Minetest-WorldEdit |    |
| xcompat                | https://github.com/mt-mods/xcompat.git |    |